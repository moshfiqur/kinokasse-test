'''
The main entry point to run the app. Sample run for dev env:
FLASK_APP=run.py FLASK_ENV=development FLASK_DEBUG=1 python -m flask run
'''

from flask import Flask, jsonify, request, render_template
from kinokasseapp import app
from kinokasseapp import routes

home_route = '/'

if __name__ == '__main__':
    app.run(debug=True)
