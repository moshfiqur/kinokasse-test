"""
This module works as (file) database interface. For the simplicity 
we have used local files to save related data necessary for functioning
the site. This module provides methods to execute different data 
retrieval calls. 
"""

import os
from kinokasseapp import app

def get_shows():
    """
    Get all the available shows info
    """
    show_info = ''
    with open(os.path.join(app.config['DATA_FOLDER'], 'shows.csv'), 'r') as f:
        show_info = str(f.read()).splitlines()
    
    movie_shows = []

    for index, row in enumerate(show_info):
        current_movie = ''
        
        if index % 2 == 0:
            movie_show = {}
            # This row is movie info
            # Split them by ;
            movie_info = row.split(';')
            movie_show['id'] = movie_info[0]
            movie_show['name'] = movie_info[1]
            movie_show['poster'] = movie_info[2]
            movie_show['plot'] = movie_info[3]
            movie_show['genre'] = movie_info[4]
            
            # This row is show info
            row = show_info[index+1]
            
            movie_show['shows'] = {
                'Mo': [],
                'Di': [],
                'Mi': [],
                'Do': [],
                'Fr': [],
                'Sa': [],
                'So': []
            }
            for s_inf in row.split(','):
                s_inf = s_inf.split('/')
                show_details = {
                    'time': s_inf[1],
                    'theater': s_inf[2],
                    'price': s_inf[3]
                }
                movie_show['shows'][s_inf[0]].append(show_details)
            
            movie_shows.append(movie_show)
    
    return movie_shows


def get_show(movie_id):
    """
    Get a specific show info by given movie_id
    """
    show_info = ''
    with open(os.path.join(app.config['DATA_FOLDER'], 'shows.csv'), 'r') as f:
        show_info = str(f.read()).splitlines()
    
    movie_show = {}

    for index, row in enumerate(show_info):
        if index % 2 == 0:
            # This row is movie info
            # Split them by ;
            movie_info = row.split(';')

            if movie_id != movie_info[0]:
                continue

            movie_show['id'] = movie_info[0]
            movie_show['name'] = movie_info[1]
            movie_show['poster'] = movie_info[2]
            movie_show['plot'] = movie_info[3]
            movie_show['genre'] = movie_info[4]
            
            # This row is show show info
            # Split them by ,
            row = show_info[index+1]
            
            movie_show['shows'] = {
                'Mo': [],
                'Di': [],
                'Mi': [],
                'Do': [],
                'Fr': [],
                'Sa': [],
                'So': []
            }
            
            for s_inf in row.split(','):
                s_inf = s_inf.split('/')
                show_details = {
                    'time': s_inf[1],
                    'theater': s_inf[2],
                    'price': s_inf[3]
                }
                movie_show['shows'][s_inf[0]].append(show_details)
    
    return movie_show


def get_seatplan(movie_id, show_day, show_time, theater):
    """
    Get seat plan for a specific movie show in a specific theater.
    This method will try to open the seatplan file for a specific show
    for a specific movie in a theater. If the seatplan has not been 
    created yet, for example, when the first ticket is being booked,
    it will read the default seat plan for that theater and copy it
    for the specific movie show.
    """
    seatplan = []
    
    seatplan_file = os.path.join(app.config['DATA_FOLDER'], movie_id+'_'+show_day+'_'+show_time.replace(':', '')+'_'+theater.replace(' ', '')+'.txt')

    if not os.path.isfile(seatplan_file):
        # Read the structure of the theater from default template file for that theater
        with open(os.path.join(app.config['DATA_FOLDER'], theater+'.txt'), 'r') as f:
            seatplan_frame = f.read()
    else:
        with open(seatplan_file, 'r') as f:
            seatplan_frame = f.read()
    
    seatplan = [list(seat) for seat in [seat_rows for seat_rows in seatplan_frame.splitlines()]]

    return seatplan


def save_seatplan(seatplan, movie_id, show_day, show_time, theater):
    """
    After user selected seats and perform the full booking process
    with payment, then we save the updated seatplan marked with 
    booked seat, so that next time it cannot be booked again.
    """
    seatplan_file = os.path.join(app.config['DATA_FOLDER'], movie_id+'_'+show_day+'_'+show_time.replace(':', '')+'_'+theater.replace(' ', '')+'.txt')

    str_seat_rows = []
    for seat_rows in seatplan:
        str_seat_rows.append(''.join(seat_rows))
    
    seatplan = '\n'.join(str_seat_rows)
    
    with open(seatplan_file, 'w') as f:
        f.write(seatplan)
