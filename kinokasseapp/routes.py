"""
This routes module implements features for all the available routes in the site.
"""

from flask import Flask, jsonify, request, render_template
from kinokasseapp import app
from kinokasseapp import db
from fpdf import FPDF
import os
from datetime import datetime

@app.route('/')
@app.route('/index')
def index():
    """
    Site endpoint for home page
    """
    return render_template('index.html', 
        movie_shows=db.get_shows())


@app.route('/showdetails/<movie_id>')
def showdetails(movie_id):
    """
    Site endpoint to show the details of a movie and 
    the available show times
    """
    return render_template('showdetails.html', movie_show=db.get_show(movie_id), movie_id=movie_id)


@app.route('/seatplan')
def seatplan():
    """
    Site endpoint to show the seat plan for a selected
    movie and show time.
    """
    movie_id = request.args.get('movie_id')
    show_day = request.args.get('show_day')
    show_time = request.args.get('show_time')
    theater = request.args.get('theater')
    movie_show=db.get_show(movie_id)
    
    price = 0
    for show in movie_show['shows'][show_day]:
        if show['time'] == show_time and show['theater'] == theater:
            price = show['price']

    seatplan = db.get_seatplan(movie_id=movie_id, show_day=show_day, show_time=show_time, theater=theater)
    return render_template('seatplan.html', movie_id=movie_id, 
        show_day=show_day, show_time=show_time, 
        theater=theater, price=price, 
        seatplan=seatplan, movie_show=movie_show)


@app.route('/payment', methods=['POST'])
def payment():
    """
    Site endpoint to allow the user pay for selected
    movie show.
    """
    movie_id = request.form.get('movie_id')
    show_day = request.form.get('show_day')
    show_time = request.form.get('show_time')
    theater = request.form.get('theater')
    price = request.form.get('price')
    total_price=request.form.get('total_price')
    booked_seats = request.form.get('booked_seats')
    movie_show=db.get_show(movie_id)
    return render_template('payment.html', movie_id=movie_id, 
        show_day=show_day, show_time=show_time, 
        theater=theater, price=price, 
        total_price=total_price, booked_seats=booked_seats,
        movie_show=movie_show)


@app.route('/process_payment', methods=['POST'])
def process_payment():
    """
    Site endpoint which lets the user download his ticket.
    This method also process the payment and book the 
    seletected seats.
    """
    movie_id = request.form.get('movie_id')
    show_day = request.form.get('show_day')
    show_time = request.form.get('show_time')
    theater = request.form.get('theater')
    price = request.form.get('price')
    total_price=request.form.get('total_price')
    booked_seats = request.form.get('booked_seats')
    card_owner = request.form.get('card_owner')
    card_number = request.form.get('card_number')
    validity_month = request.form.get('validity_month')
    validity_year = request.form.get('validity_year')
    cvc_code = request.form.get('cvc_code')

    movie_show=db.get_show(movie_id)
    
    # Book the seat in seat plan
    seat_info = []
    seatplan = db.get_seatplan(movie_id, show_day, show_time, theater)
    for seat in booked_seats.split(','):
        row_index, seat_index = seat.split('_')
        seatplan[int(row_index)][int(seat_index)] = 'x'
        seat_info.append('R'+str(int(row_index)+1)+' S'+str(int(seat_index)+1))
    db.save_seatplan(seatplan, movie_id, show_day, show_time, theater)

    booking_time = datetime.now()
    ticket_file_name = card_owner.replace(' ', '')+'_'+movie_id \
        +'_'+show_day+'_'+show_time.replace(':', '')+'_'+theater.replace(' ', '') \
        +'_'+'{0:%d%m%Y%H%M%S}'.format(booking_time)+'.pdf'    
    
    # Generate the ticket pdf for user
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font('Arial', size=16)
    pdf.cell(0, 17, txt='KinoKasse', border=1, ln=1)
    pdf.set_font_size(12)
    
    pdf.cell(pdf.w/3, 14, txt='Movie Name', border='B', ln=0)
    pdf.cell(0, 14, txt=movie_show['name'], border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Movie Time', border='B', ln=0)
    pdf.cell(0, 14, txt=show_day+' '+show_time, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Theater', border='B', ln=0)
    pdf.cell(0, 14, txt=theater, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Seat', border='B', ln=0)
    pdf.cell(0, 14, txt=', '.join(seat_info), border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Booking Person', border='B', ln=0)
    pdf.cell(0, 14, txt=card_owner, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Booking Time', border='B', ln=0)
    pdf.cell(0, 14, txt='{0:%d/%m/%Y %H:%M}'.format(booking_time), border='B', ln=1)

    pdf.output(os.path.join(app.config['DOWNLOAD_FOLDER'], 'tickets', ticket_file_name))

    # Generate the booking details pdf for kinokasse
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font('Arial', size=16)
    pdf.cell(0, 17, txt='KinoKasse', border=1, ln=1)
    pdf.set_font_size(12)
    
    pdf.cell(pdf.w/3, 14, txt='Movie Name', border='B', ln=0)
    pdf.cell(0, 14, txt=movie_show['name'], border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Movie Time', border='B', ln=0)
    pdf.cell(0, 14, txt=show_day+' '+show_time, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Theater', border='B', ln=0)
    pdf.cell(0, 14, txt=theater, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Seat', border='B', ln=0)
    pdf.cell(0, 14, txt=', '.join(seat_info), border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Booking Time', border='B', ln=0)
    pdf.cell(0, 14, txt='{0:%d/%m/%Y %H:%M:%S}'.format(booking_time), border='B', ln=1)
    
    pdf.ln(16)
    pdf.set_font_size(14)
    pdf.cell(0, 14, txt='Payment Details', border=1, ln=1)
    
    pdf.set_font_size(12)
    pdf.cell(pdf.w/3, 14, txt='Card Owner', border='B', ln=0)
    pdf.cell(0, 14, txt=card_owner, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Card Number', border='B', ln=0)
    pdf.cell(0, 14, txt=card_number, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='Validity', border='B', ln=0)
    pdf.cell(0, 14, txt=validity_month+'/'+validity_year, border='B', ln=1)
    pdf.cell(pdf.w/3, 14, txt='CVC Code', border='B', ln=0)
    pdf.cell(0, 14, txt=cvc_code, border='B', ln=1)

    pdf.output(os.path.join(app.config['DATA_FOLDER'], 'bookings', ticket_file_name))

    return render_template('payment_processed.html', status='success', ticket=ticket_file_name)
