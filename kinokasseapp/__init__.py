"""
The core module to init an instance of our site app.
"""
import os
from flask import Flask, jsonify, request, render_template
from .config import app_config

def create_app(test_config=None):
    """
    create and configure the app
    """
    app = Flask(__name__, instance_relative_config=True)

    if env_name is None:
        app.config.from_object(app_config['development'])
    else:
        app.config.from_object(app_config[env_name])

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.config['CACHE_TYPE'] = "null"
    app.config['DOWNLOAD_FOLDER'] = '/var/www/html/wibas/kinokasseapp/static/downloads'
    app.config['DATA_FOLDER'] = '/var/www/html/wibas/data'

    return app

# Get app instance
if 'FLASK_ENV' in os.environ:
    env_name = os.environ['FLASK_ENV']
else:
    env_name = 'development'

app = create_app(env_name)
