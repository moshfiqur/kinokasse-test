"""
This module contains typical dev/prod related 
config values for the site.
"""

import os

class dev(object):
    """
    Environment config for development
    """
    DEBUG = True
    TESTING = True
    SECRET_KEY = 'secret key for dev env'

class prod(object):
    """
    Enviroment config for production
    """
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'secret key for prod env'

app_config = {
    'development': dev,
    'production': prod
}
