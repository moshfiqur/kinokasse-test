$(function() {
    /**
     * Callback function for the event whenever user 
     * select/deselect a seat on the seatplan.
     */
    $('.js__seat_selection_check').change(function() {
        update_total_price($(this));
        update_booked_seats();
    });

    /**
     * Callback function whenever user enters/deletes 
     * sometime from the payment form.
     */
    $('.js__form_payment input').on('input', function() {
        if (
            $('input[name=card_owner]').val() == '' || 
            $('input[name=card_number]').val() == '' || 
            $('input[name=cvc_code]').val() == ''
        ) {
            $('.js__button_pay').attr('disabled', true);
            return false;
        }

        $('.js__button_pay').attr('disabled', false);
    });
});

/**
 * Update the booked seats status after user selects/deselects
 * a seat of the seatplan.
 */
function update_booked_seats() {
    var booked_seats = '';
    var enable_book_button = false;
    
    $('.js__seat_selection_check').each(function() {
        if ($(this).is(':checked')) {
            row_index = $(this).data('row_index');
            seat_index = $(this).data('seat_index');
            
            if (booked_seats == '') {
                booked_seats += row_index+'_'+seat_index
            } else {
                booked_seats += ','+row_index+'_'+seat_index
            }

            enable_book_button = true;
        }
    });

    if (enable_book_button) {
        $('.js__book_button').attr('disabled', false);
    } else {
        $('.js__book_button').attr('disabled', true);
    }

    $('input[name=booked_seats]').val(booked_seats);
}

/**
 * Update the calculated total price depending on users'
 * selected seats.
 */
function update_total_price(cb_obj) {
    var ticket_price = $('input[name=price]').val();
    var total_price = $('.js__total_price').data('total_price');

    if (cb_obj.is(':checked')) {
        total_price = +total_price + +ticket_price;
    } else {
        total_price = total_price - ticket_price;
    }
    
    total_price = total_price.toFixed(2);

    $('.js__total_price').data('total_price', total_price);
    $('input[name=total_price]').val(total_price);
    $('.js__total_price').text(total_price + ' EUR');
}
