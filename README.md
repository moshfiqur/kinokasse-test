# kinokasse Test

This project implements a prototype for the ticket booking system for Kinokasse.

## Installation
First we need to clone this git repo to get the code:
```
$ git clone https://gitlab.com/moshfiqur/kinokasse-test.git
$ cd kinokasse-test
```

Before proceeding to install all the required python module, we might like to create a virtual environment for this project so that it does not interfere our other existing python projects. To create a virtual environment, we can take the following steps:

```
$ mkdir venv
$ virtualenv -p python3 venv/kinokasse
$ source venv/kinokasse/bin/activate
```

This prototype is implemented using [flask][1] python microframework. To make the project run we need flask and other of its dependencies. For that reason we need to install the following python modules:
```
Click==7.0
Flask==1.0.2
fpdf==1.7.2
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.0
Werkzeug==0.14.1
```

Those modules are given in `requirements.txt` so we can install all the dependencies by running this single command in terminal:

```
$ pip install -r requirements.txt
```

## Settings
Before we can run our app, we have to setup some settings, for example, file download or data folder path in the `kinokasseapp/__init__.py` script. The settings look like this:

```python
app.config['DOWNLOAD_FOLDER'] = '/var/www/html/wibas/kinokasseapp/static/downloads'
app.config['DATA_FOLDER'] = '/var/www/html/wibas/data'
```

## Start the development server
While we are still in the `kinokasse-test` directory, we can run the development server to test the site features. To do so, this command need to be executed:
```
$ FLASK_APP=run.py FLASK_ENV=development FLASK_DEBUG=1 python -m flask run
```
This command is doing the following things:
- FLASK_APP: Specifies which script to run to load the application
- FLASK_ENV: Specifies that we are running the application in development mode.
- FLASK_DEBUG: Specifies that we are running the application with debugging enabled

So, to run the application in `production` settings, we can execute the command:
```
$ FLASK_APP=run.py FLASK_ENV=production FLASK_DEBUG=0 python -m flask run
```
However, we should not run the development server in production environment, we should use a production WSGI server instead.

When everything goes ok so far, we can test the API home endpoint using browser by pointing to url [http://localhost:5000/][2].

If our installation and other settings were successfully setup, we should see the home page of our site. Below there are some screenshots provided to show how all the pages look like following the ticket booking process.

## Website screenshots

### Home page
![Home page](data/presentation/home.jpg)

### Movie shows
![Movie shows](data/presentation/shows.jpg)

### Seat plan (choose seat)
![Seat plan (choose seat)](data/presentation/seatplan.jpg)

### Payment
![Payment](data/presentation/payment.jpg)

### Booking success
![Booking success](data/presentation/booking_success.jpg)

## Sample outputs
There are some sample outputs of booking info (for the internal user of kinokasse) and tickets downloadable by end users available for checking. The booking info can be found here [https://gitlab.com/moshfiqur/kinokasse-test/tree/master/data/bookings][4] and generated tickets are downloadable from here [https://gitlab.com/moshfiqur/kinokasse-test/tree/master/kinokasseapp/static/downloads/tickets][5]

## Disclaimer
All the movie plot info and posters were collected from [The Movie DB (TMDB)][3] using their free and open API.

[1]: http://flask.pocoo.org/
[2]: http://localhost:5000/
[3]: https://www.themoviedb.org/
[4]: https://gitlab.com/moshfiqur/kinokasse-test/tree/master/data/bookings
[5]: https://gitlab.com/moshfiqur/kinokasse-test/tree/master/kinokasseapp/static/downloads/tickets